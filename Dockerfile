FROM debian:stable-slim
LABEL MAINTAINER chevdor@gmail.com

ENV CRON='0 * * * *'
ENV DB=speedtest
ENV INFLUXDB_HOST=http://localhost
ENV INFLUXDB_PORT=8086
ENV INFLUXDB_USER=speedtest
# ENV INFLUXDB_BUCKET=speedtest
# ENV INFLUXDB_TOKEN=
ENV INFLUXDB_PASS=
ENV TARGET=/etc/cron.d/speedtest-cron
ENV SERVER=

RUN apt-get update && \
    apt-get install -y \
        apt-transport-https dirmngr curl jq bc && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 379CE192D401AB61 && \
    curl -s https://packagecloud.io/install/repositories/ookla/speedtest-cli/script.deb.sh | bash && \
    apt-get install speedtest cron && \
    rm -rf /var/lib/apt/lists/*

ADD setup.sh /usr/local/bin
ADD run.sh /usr/local/bin
ADD start.sh /usr/local/bin
RUN touch /var/log/cron.log

CMD ["/usr/local/bin/start.sh"]
